﻿namespace LayeredAttributes
{
    /// <summary>
    /// Parameter struct for AddLayeredEffect(...)
    /// </summary>
    public struct LayeredEffectDefinition
    {
        /// <summary>
        /// Which attribute this layered effect applies to.
        /// </summary>
        public AttributeKey Attribute { get; set; }

        /// <summary>
        /// What mathematical or bitwise operation this layer performs.
        /// See EffectOperation for details.
        /// </summary>
        public EffectOperation Operation { get; set; }

        /// <summary>
        /// The operand used for this layered effect's Operation.
        /// For example, if Operation is EffectOperation.Add, this is the
        /// amount that is added.
        /// </summary>
        public int Modification { get; set; }

        /// <summary>
        /// Which layer to apply this effect in. Smaller numbered layers
        /// get applied first. Layered effects with the same layer get applied
        /// in the order that they were added. (timestamp order)
        /// </summary>
        public int Layer { get; set; }
    }

}
