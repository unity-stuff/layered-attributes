﻿using System;

namespace LayeredAttributes
{
    public static class LayeredEffectApplicator
    {
        public static int ApplyLayeredEffect(int attributeValue, LayeredEffectDefinition layeredEffect)
        {
            int computedValue = attributeValue;

            switch (layeredEffect.Operation)
            {
                case EffectOperation.Set:
                    computedValue = layeredEffect.Modification;
                    break;
                case EffectOperation.Add:
                    computedValue = attributeValue + layeredEffect.Modification;
                    break;
                case EffectOperation.Subtract:
                    computedValue = attributeValue - layeredEffect.Modification;
                    break;
                case EffectOperation.Multiply:
                    computedValue = attributeValue * layeredEffect.Modification;
                    break;
                case EffectOperation.BitwiseAnd:
                    computedValue = attributeValue & layeredEffect.Modification;
                    break;
                case EffectOperation.BitwiseOr:
                    computedValue = attributeValue | layeredEffect.Modification;
                    break;
                case EffectOperation.BitwiseXor:
                    computedValue = attributeValue ^ layeredEffect.Modification;
                    break;
                case EffectOperation.Invalid:
                    throw new ArgumentOutOfRangeException($"Operation {layeredEffect.Operation} is invalid.");
            }

            return computedValue;
        }
    }
}
