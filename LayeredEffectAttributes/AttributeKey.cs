﻿namespace LayeredAttributes
{
    public enum AttributeKey
    {
        Invalid = 0,
        Power,
        Toughness,
        Loyalty,
        Color,
        Types,
        Subtypes,
        Supertypes,
        ConvertedManaCost,
        Controller
    }
}