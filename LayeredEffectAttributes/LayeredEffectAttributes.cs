﻿using System.Collections.Generic;
using System.Linq;

namespace LayeredAttributes
{
    /// <summary>
    /// Implements the <see cref="ILayeredAttributes"/> interface. 
    /// Manages a collection of layered effect attributes.
    /// </summary>
    public class LayeredEffectAttributes : ILayeredAttributes
    {
        Dictionary<AttributeKey, LayeredAttribute> _layeredAttributes;

        /// <summary>
        /// Constructor - instantiate list of <see cref="LayeredAttribute"/>.
        /// </summary>
        public LayeredEffectAttributes()
        {
            _layeredAttributes = new Dictionary<AttributeKey, LayeredAttribute>();
        }

        /// <summary>
        /// Applies a new layered effect to this object's attributes. See
        /// LayeredEffectDefinition for details on how layered effects are
        /// applied. Note that any number of layered effects may be applied
        /// at any given time. Also note that layered effects are not necessarily
        /// applied in the same order they were added. (see LayeredEffectDefinition.Layer)
        /// </summary>
        /// <param name="effect">The new layered effect to apply.</param>
        public void AddLayeredEffect(LayeredEffectDefinition effect)
        {
            GetLayeredAttribute(effect.Attribute).AddLayeredEffect(effect);
        }

        /// <summary>
        /// Removes all layered effects from this object. After this call,
        /// all current attributes will be equal to the base attributes.
        /// </summary>
        public void ClearLayeredEffects()
        {
            foreach(LayeredAttribute attribute in _layeredAttributes.Values)
            {
                attribute.ClearLayeredEffects();
            }
        }

        /// <summary>
        /// Return the current value for an attribute on this object. Will
        /// be equal to the base value, modified by any applicable layered
        /// effects.
        /// </summary>
        /// <param name="attKey">The attribute being read.</param>
        /// <returns>The current value of the attribute, accounting for all layered effects.</returns>
        public int GetCurrentAttribute(AttributeKey attKey)
        {

            return GetLayeredAttribute(attKey).CurrentValue;
        }

        /// <summary>
        /// Set the base value for an attribute on this object. All base values
        /// default to 0 until set. Note that resetting a base attribute does not
        /// alter any existing layered effects.
        /// </summary>
        /// <param name="attKey">The attribute being set.</param>
        /// <param name="value">The new base value.</param>
        public void SetBaseAttribute(AttributeKey attKey, int value)
        {
            GetLayeredAttribute(attKey).BaseValue = value;
        }

        private LayeredAttribute GetLayeredAttribute(AttributeKey key)
        {
            if (!_layeredAttributes.ContainsKey(key))
            {
                LayeredAttribute attribute = new LayeredAttribute(key);
                _layeredAttributes.Add(key, attribute);
            }
            return _layeredAttributes[key];
        }
    }
}
