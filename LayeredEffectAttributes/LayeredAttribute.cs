﻿using System;
using System.Collections.Generic;

namespace LayeredAttributes
{
    public class LayeredAttribute
    {
        /// <summary>
        /// Which attribute is this?
        /// </summary>
        public AttributeKey Key { get; private set; }

        /// <summary>
        /// The base value for this attribute
        /// </summary>
        public int BaseValue
        {
            get
            {
                return _baseValue;
            }
            set 
            {
                _baseValue = value;
                _computedValueCacheInvalid = true;
            } 
        }

        private List<LayeredEffectDefinition> _layeredEffects;
        private int _computedValueCache;
        private int _baseValue;
        private bool _computedValueCacheInvalid;

        /// <summary>
        /// Constructor. Instantiate list of layered effect definitions.
        /// </summary>
        public LayeredAttribute()
        {
            _layeredEffects = new List<LayeredEffectDefinition>();
            _computedValueCacheInvalid = true;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="key">Attribute key we're constructing</param>
        public LayeredAttribute(AttributeKey key) : this()
        {
            Key = key;
            BaseValue = 0;
        }

        /// <summary>
        /// Get the current value for this attribute, including the
        /// application of any layered effects. 
        /// </summary>
        /// <remarks>We calculate the current value on demand and cache the value.
        /// The application of any new effects will invalidate the cached value
        /// requiring us to recompute it the next time we request the current value.</remarks>
        public int CurrentValue
        {
            get
            {
                // Only compute value if our current value has been invalidated
                // by adding a new effect.
                if (_computedValueCacheInvalid)
                {
                    return ComputeCurrentValue();
                }
                return _computedValueCache;
            }
        }

        /// <summary>
        /// Add a layered effect to this attribute.
        /// </summary>
        /// <param name="layeredEffect">The <see cref="LayeredEffectDefinition"/> to be added.</param>
        public void AddLayeredEffect(LayeredEffectDefinition layeredEffect)
        {
            _computedValueCacheInvalid = true;
            int index = GetLayerIndex(layeredEffect.Layer);
            if (index > -1)
            {
                _layeredEffects.Insert(index, layeredEffect);
                return;
            }
            _layeredEffects.Add(layeredEffect);
        }

        /// <summary>
        /// Clear all layered effects from this attribute.
        /// This does not affect the base value.
        /// </summary>
        public void ClearLayeredEffects()
        {
            _layeredEffects.Clear();
            _computedValueCacheInvalid = true;
        }

        private int ComputeCurrentValue()
        {
            int currentValue = BaseValue;
            foreach (var effect in _layeredEffects)
            {
                currentValue = LayeredEffectApplicator.ApplyLayeredEffect(currentValue, effect);
            }
            _computedValueCache = currentValue;
            _computedValueCacheInvalid = false;
            return _computedValueCache;
        }

        private int GetLayerIndex(int layer)
        {
            return _layeredEffects.FindIndex(effect => effect.Layer > layer);
        }

    }
}
