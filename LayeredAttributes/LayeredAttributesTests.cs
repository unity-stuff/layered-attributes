﻿using FluentAssertions;
using LayeredAttributes;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LayeredAttributesTests
{
    [TestClass]
    public class LayeredAttributesTests
    {
        private LayeredEffectAttributes _attributes;

        [TestInitialize]
        public void Initialize()
        {
            _attributes = CreateAttributesWithBaseValues();
        }

        [TestMethod]
        public void ShouldReturnBaseValuesForAllAttributes()
        {
            // Arrange (handled by Initialize)

            // Act (no action)

            // Assert
            _attributes.GetCurrentAttribute(AttributeKey.Power).Should().Be(1);
            _attributes.GetCurrentAttribute(AttributeKey.Toughness).Should().Be(2);
            _attributes.GetCurrentAttribute(AttributeKey.Loyalty).Should().Be(3);
        }

        [TestMethod]
        [DataRow(AttributeKey.Power, EffectOperation.Add, 1, 2)]
        [DataRow(AttributeKey.Toughness, EffectOperation.Subtract, 1, 1)]
        [DataRow(AttributeKey.Loyalty, EffectOperation.Multiply, 2, 6)]
        public void AttributesWithSingleLayeredEffectShouldComputeCorrectly(AttributeKey attribute, EffectOperation operation, int modification, int expectedResult)
        {
            // Arrange (handled by initialize)

            // Act
            _attributes.AddLayeredEffect(new LayeredEffectDefinition
            {
                Attribute = attribute,
                Operation = operation,
                Modification = modification
            });

            // Assert
            _attributes.GetCurrentAttribute(attribute).Should().Be(expectedResult);
        }

        [TestMethod]
        [DataRow(AttributeKey.Power,
            EffectOperation.Add, 1,
            EffectOperation.Multiply, 2,
            EffectOperation.Subtract, 2,
            2)]
        public void AttributesWithMultipleLayeredEffectsShouldComputeCorrectly(
            AttributeKey attribute, 
            EffectOperation operation1, int modification1, 
            EffectOperation operation2, int modification2, 
            EffectOperation operation3, int modification3,
            int expectedResult
            )
        {
            // Arrange (handled by initialize)

            // Act
            _attributes.AddLayeredEffect(new LayeredEffectDefinition
            {
                Attribute = attribute,
                Operation = operation1,
                Modification = modification1
            });
            _attributes.AddLayeredEffect(new LayeredEffectDefinition
            {
                Attribute = attribute,
                Operation = operation2,
                Modification = modification2
            });
            _attributes.AddLayeredEffect(new LayeredEffectDefinition
            {
                Attribute = attribute,
                Operation = operation3,
                Modification = modification3
            });

            // Assert
            _attributes.GetCurrentAttribute(attribute).Should().Be(expectedResult);
        }

        [TestMethod]
        [DataRow(
            AttributeKey.Toughness,
            5,                          // base value
            2,                          // layer 1
            EffectOperation.Multiply,   // operation 1
            3,                          // modification 1
            1,                          // layer 2
            EffectOperation.Add,        // operation 2
            1,                          // modification 2
            1,                          // layer 3
            EffectOperation.Subtract,   // operation 3
            1,                          // modfiication 3
            15                          // expected result
            )]
        public void EffectsLayerOrderShouldComputeCorrectly(
            AttributeKey attribute,
            int baseValue,
            int layer1,
            EffectOperation operation1,
            int modification1,
            int layer2,
            EffectOperation operation2,
            int modification2,
            int layer3,
            EffectOperation operation3,
            int modification3,
            int expectedResult
            )
        {
            // Arrange (handled by initialize)

            // Act
            _attributes.SetBaseAttribute(attribute, baseValue);
            _attributes.AddLayeredEffect(new LayeredEffectDefinition
            {
                Attribute = attribute,
                Layer = layer1,
                Operation = operation1,
                Modification = modification1
            });
            _attributes.AddLayeredEffect(new LayeredEffectDefinition
            {
                Attribute = attribute,
                Layer = layer2,
                Operation = operation2,
                Modification = modification2
            });
            _attributes.AddLayeredEffect(new LayeredEffectDefinition
            {
                Attribute = attribute,
                Layer = layer3,
                Operation = operation3,
                Modification = modification3
            });

            // Assert
            _attributes.GetCurrentAttribute(attribute).Should().Be(expectedResult);
        }

        [TestMethod]
        public void ClearingLayeredEffectsShouldReturnBaseValue()
        {
            // Arrange (handled by Initialize)

            // Act
            _attributes.SetBaseAttribute(AttributeKey.Toughness, 5);
            _attributes.AddLayeredEffect(new LayeredEffectDefinition
            {
                Attribute = AttributeKey.Toughness,
                Operation = EffectOperation.Multiply,
                Modification = 2
            });
            _attributes.SetBaseAttribute(AttributeKey.Loyalty, 10);
            _attributes.AddLayeredEffect(new LayeredEffectDefinition
            {
                Attribute = AttributeKey.Loyalty,
                Operation = EffectOperation.Add,
                Modification = 2
            });

            _attributes.ClearLayeredEffects();

            // Assert
            _attributes.GetCurrentAttribute(AttributeKey.Toughness).Should().Be(5);
            _attributes.GetCurrentAttribute(AttributeKey.Loyalty).Should().Be(10);
            _attributes.GetCurrentAttribute(AttributeKey.Power).Should().Be(1);
        }

        [TestMethod]
        public void SetShouldOverrideValue()
        {
            // Arrange (handled by Initialize)

            // Act
            _attributes.SetBaseAttribute(AttributeKey.Toughness, 5);
            _attributes.AddLayeredEffect(new LayeredEffectDefinition
            {
                Attribute = AttributeKey.Toughness,
                Operation = EffectOperation.Multiply,
                Modification = 2
            });
            _attributes.AddLayeredEffect(new LayeredEffectDefinition
            {
                Attribute = AttributeKey.Toughness,
                Operation = EffectOperation.Set,
                Modification = 1
            });
            _attributes.AddLayeredEffect(new LayeredEffectDefinition
            {
                Attribute = AttributeKey.Toughness,
                Operation = EffectOperation.Add,
                Modification = 1
            });

            _attributes.GetCurrentAttribute(AttributeKey.Toughness).Should().Be(2);
        }

        private LayeredEffectAttributes CreateAttributesWithBaseValues()
        {
            LayeredEffectAttributes attributes = new LayeredEffectAttributes();
            attributes.SetBaseAttribute(AttributeKey.Power, 1);
            attributes.SetBaseAttribute(AttributeKey.Toughness, 2);
            attributes.SetBaseAttribute(AttributeKey.Loyalty, 3);
            return attributes;
        }

    }
}
